// ==UserScript==
// @name        Open AWS console links in the appropriate Multi-Account Container tab
// @namespace   Violentmonkey Scripts
// @match       https://*.console.aws.amazon.com/
// @match       https://signin.aws.amazon.com/signin
// https://signin.aws.amazon.com/signin*
// @grant       none
// @version     1.0
// @author      Lyle Hanson
// @description Examines AWS Console links to see if they contain an account ID. If so, open the link in an appropriate Multi-Account Container tab.
// ==/UserScript==
console.log("URL: " + window.location.href)

// Sample login URL: https%3A%2F%2Fus-east-1.console.aws.amazon.com%2Fcloudwatch%2Fhome%3Fregion%3Dus-east-1%26state%3DhashArgs%2523logsV2%253Alog-groups%252Flog-group%252Farn%2524253Aaws%2524253Alogs%2524253Aus-east-1%2524253A891257625688%2524253Alog-group%2524253A%2524252Fecs%2524252Fcaos-soap-1.8%252Flog-events%252FalarmsTest%25243Fstart%25243D1705592573798%252426refEventId%25243D38035985400775236013754621068019808039031748927327305728%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fcloudwatch&forceMobileApp=0&code_challenge=aIrqvzh68FOf-xun59r_Ymjs_-WoOLY4pIXoMgPhIA0&code_challenge_method=SHA-256
